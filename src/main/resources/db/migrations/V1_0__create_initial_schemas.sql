CREATE TABLE account (
    id serial NOT NULL,
    username character varying(32) NOT NULL,
    password character varying(1024) NOT NULL,
    role character varying(32) NOT NULL,
    PRIMARY KEY (id)
);