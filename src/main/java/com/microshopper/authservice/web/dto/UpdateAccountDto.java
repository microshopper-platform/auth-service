package com.microshopper.authservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "AccountUpdate")
public class UpdateAccountDto {

  private String username;

  private String email;

  private RoleDto role;
}
