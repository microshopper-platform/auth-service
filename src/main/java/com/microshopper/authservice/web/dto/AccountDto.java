package com.microshopper.authservice.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "AccountRegistration")
public class AccountDto {

  private String username;

  private String password;

  private String email;

  private RoleDto role;

  @JsonProperty("id")
  private Long externalId;
}
