package com.microshopper.authservice.web.controller;

import com.microshopper.authservice.service.AccountService;
import com.microshopper.authservice.web.dto.AccountDto;
import com.microshopper.authservice.web.dto.UpdateAccountDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "auth", description = "The Auth API")
public class AuthController {

  private static final String BASE_URL = "/api/auth";

  private final AccountService accountService;

  @Autowired
  public AuthController(AccountService accountService) {
    this.accountService = accountService;
  }

  @PostMapping(value = BASE_URL + "/accounts")
  @ResponseStatus(value = HttpStatus.CREATED)
  public void registerAccount(@RequestBody AccountDto accountDto) {

    accountService.addAccount(accountDto);
  }

  @PutMapping(value = BASE_URL + "/accounts")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateAccount(@RequestBody UpdateAccountDto updateAccountDto) {

    accountService.updateAccount(updateAccountDto);
  }
}
