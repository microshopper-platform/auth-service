package com.microshopper.authservice.configuration;

import com.microshopper.authservice.model.Account;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

public class CustomTokenEnhancer implements TokenEnhancer {

  @Override
  public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

    Map<String, Object> additionalInformation = new HashMap<>();

    Account account = (Account) authentication.getPrincipal();

    additionalInformation.put("id", account.getId());
    additionalInformation.put("username", account.getUsername());
    additionalInformation.put("role", account.getRole());
    additionalInformation.put("externalId", account.getExternalId());
    additionalInformation.put("email", account.getEmail());

    ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);

    return accessToken;
  }
}
