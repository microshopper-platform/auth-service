package com.microshopper.authservice.configuration;

import com.microshopper.authservice.service.AccountUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@Order(1)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final AccountUserDetailsService accountUserDetailsService;

  @Autowired
  public WebSecurityConfig(AccountUserDetailsService accountUserDetailsService) {
    this.accountUserDetailsService = accountUserDetailsService;
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http.authorizeRequests()
      .antMatchers("/tokens/**").permitAll()
      .antMatchers(HttpMethod.POST, "/api/auth/accounts").permitAll()
      .antMatchers(HttpMethod.PUT, "/api/auth/accounts").permitAll()
      .antMatchers("/v3/**").permitAll()
      .antMatchers("/swagger-ui.html").permitAll()
      .antMatchers("/swagger-ui/**").permitAll()
      .anyRequest().authenticated()
      .and().csrf().disable();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(accountUserDetailsService)
      .passwordEncoder(passwordEncoder());
  }

  @Override
  @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
