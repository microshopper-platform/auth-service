package com.microshopper.authservice.model;

public enum Role {

  ROLE_USER,

  ROLE_MANUFACTURER_ADMIN,

  ROLE_MANUFACTURER_TRANSPORTER;
}
