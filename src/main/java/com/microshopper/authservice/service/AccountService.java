package com.microshopper.authservice.service;

import com.microshopper.authservice.web.dto.AccountDto;
import com.microshopper.authservice.web.dto.UpdateAccountDto;

public interface AccountService {

  void addAccount(AccountDto accountDto);

  void updateAccount(UpdateAccountDto updateAccountDto);
}
