package com.microshopper.authservice.service;

import com.microshopper.authservice.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AccountUserDetailsService implements UserDetailsService {

  private final AccountRepository accountRepository;

  @Autowired
  public AccountUserDetailsService(AccountRepository accountRepository) {

    this.accountRepository = accountRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    return accountRepository.findAccountByUsername(username)
      .orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found."));
  }
}
