package com.microshopper.authservice.service.impl;

import com.microshopper.authservice.exception.AccountAlreadyExistsException;
import com.microshopper.authservice.exception.AccountNotFoundException;
import com.microshopper.authservice.model.Account;
import com.microshopper.authservice.model.Role;
import com.microshopper.authservice.repository.AccountRepository;
import com.microshopper.authservice.service.AccountService;
import com.microshopper.authservice.web.dto.AccountDto;
import com.microshopper.authservice.web.dto.UpdateAccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

  private final AccountRepository accountRepository;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public AccountServiceImpl(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
    this.accountRepository = accountRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public void addAccount(AccountDto accountDto) {

    String username = accountDto.getUsername();

    Optional<Account> account = accountRepository.findAccountByUsername(username);
    account.ifPresent(
      (user) -> { throw new AccountAlreadyExistsException("Adding account failed. Account with username: " + username + " already exists.");
    });

    Account newAccount = new Account();

    String hash = passwordEncoder.encode(accountDto.getPassword());

    newAccount.setUsername(username);
    newAccount.setPassword(hash);
    newAccount.setRole(Role.valueOf(accountDto.getRole().name()));
    newAccount.setExternalId(accountDto.getExternalId());
    newAccount.setEmail(accountDto.getEmail());

    accountRepository.save(newAccount);
  }

  @Override
  public void updateAccount(UpdateAccountDto updateAccountDto) {

    String username = updateAccountDto.getUsername();

    Optional<Account> accountOptional = accountRepository.findAccountByUsername(username);

    if (accountOptional.isEmpty()) {
      throw new AccountNotFoundException("Account update failed. Account with username: " + username + " not found.");
    }

    Account account = accountOptional.get();

    account.setRole(Role.valueOf(updateAccountDto.getRole().name()));
    account.setEmail(updateAccountDto.getEmail());

    accountRepository.save(account);
  }
}
